#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

// Data for x-axis, y-axis, and z-axis
typedef struct {
  uint16_t x, y, z;
} acceleration__axis_data_s;

typedef enum {
  acceleration__memory_axis_base = 0x01,
  acceleration__memory_control = 0x2A,
  acceleration__memory_who_am_i = 0x0D,
  acceleration__PL_control = 0x11,
  acceleration__PL_status = 0x10,
  acceleration__PL_count = 0x12,
  acceleration__Stand_by = 254,
  acceleration__PL_enable = 192,
  acceleration__PL_dbnc = 5,
} acceleration__memory_e;

typedef enum {
  Portrait_UP = 0,
  Portrait_DOWN = 2,
  Landscape_RIGHT = 4,
  Landscape_LEFT = 6,
  ORIENT_ERROR = 10
} orientation_e;

/// Enables acceleration sensor data update and returns true if we successfully
/// detected the sensor
bool acceleration__init(void);
void enable_orientation(void);
orientation_e GetOrientation(void);
/// @returns data of all 3-axis of the sensor
acceleration__axis_data_s acceleration__get_data(void);
